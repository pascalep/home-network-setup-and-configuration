# Home Network Setup and configuration

Setup and configuration environment for my home network.
All depends on a publicly accessible script and a local DHCP server configuration.

The purpose is easy installation and configuration, as well as a simple disaster recovery procedure.

mc1-bootstrap
---------------
    Usage
    -----
        Run this script:
        sudo sh -c "$(curl -fsSL https://gitlab.com/pascalep/home-network-setup-and-configuration/raw/master/mc1-bootstrap)"
        or
        sudo sh -c "$(curl -fsSL https://gitlab.com/pascalep/home-network-setup-and-configuration/raw/master/mc1-bootstrap)" "" -argument
        or 
        curl -LO https://gitlab.com/pascalep/home-network-setup-and-configuration/raw/master/mc1-bootstrap && chmod +x mc1-bootstrap
        sudo ./mc1-bootstrap USER

        - if mc1-bootstrap is called as sudo user without command line option it will configure the sudo user unless there is a user configured in DHCP options
        - the commandline option USER overrides the sudo user as well as the DHCP option DHCP_USERNAME
        - Configuration can only be written to root's home directory if user root is secified on the command line
        - If the script is run from the root account and no paramter is set nor SHCP_USERNAME is configured the script prompts for a new user to create

        - The script parses the DHCP Server for information how to configure the system. If not all needed information could be gathered the script will prompt the user for missing infos.
        - If the host isn't configured in DHCP at all it will prompt for a static network configuration
        - Oh_my_ZSH will be installed and configured if either ZSH is or will be installed

        - The script can be rerun to reconfigure system if DHCP Information changed or an update is available, but it can not uninstall packages removed from DHCP options

    Purpose
    -------
        This project is meant primarly for easy and consistent installation of SBC's in a test environement.
        Other systems will be added in the future
        
    What can this script do?
    ------------------------
        This script does configure a Debian based system and install Software to it primarly based on Information from a local DHCP server.
        If the client is not configured on the DHCP server it prompts the user for required input and helps setting up the DHCP server for future reinstalls.

        It does
        - set Timezone
        - set hostname
        - Upgrade installed packages to the latest version
        - set static ip and configure resolv.conf
        - remove network manager
        - disable root account

        It can
        - Create a new user
        - Add new User to groups
        - Install Software
        - Install and configure Oh_my_ZSH
        - create unique user environemnent (not yet)
        - Provide information to configure local DHCP Server

    Prerequisites
    -------------
        All you need to use this project are:
        - Internet Access
        - A DHCP Server that allows for definiton of local options
        - at least: diff, curl, wget, awk, git (if Oh_my_ZSH is used), dhclient, sed, date, sudo,
            getent, cmp, ifup, perl 
        - At least one SBC like Raspberry, Odroid, or other with a Debian based distribution
        - The passion to troubleshoot if something doesn't work as expected.

    Todo
    ------
        - check if system is uses apt                                               (ok)
        - check for mandatory binarys to run this script, else install them         (ok)
    - option to run the script unattended                                           (ok)    unattended update not working yet
        - create unique directory and file names for setup files
        - apt_update does not run on fresh system                                   (ok)
        - Implement an option for dynamic IP address configuration
        - set Timezone                                                              (ok)
        - set hostname                                                              (ok)
        - System update                                                             (ok)
        - Software installieren                                                     (ok)
        - set static ip                                                             (ok)
        - remove network manager                                                    (ok)
        - disable root account                                                      (ok)
        - create unique user environemnent                                          (partly implemented)
        - Other packages?
        - Update .zshrc                                                             (ok)
        - combine different package installation methods in Setup Info              (ok)
        - add Oh-my-zsh to root user
        - Option to remove packages
        - Add NFS share
        - add configuration for clients with netplan
        - fix oh my zsh installation if new user is added                           (ok)
        - add existing user to groups defined with DHCP option local_groups
        - Interaction to enter missing paramters not received from dhcp             (ok)    very basic implementation
              This should also check if no hostname is send from dhcp we assume 
              dynamic ip and ask for manual configuration
        - write a log file and optimize console output  
        - configure files for user environement
               Vim, zsh, tmux, aliases, etc.
               create a routine to update configuration files
        - If the script does not run for the first time different options           (ok)    
           - only upgrade                                                           (ok)
           - create a new user, configure it (ask to add it to sudo)                (ok)
        - Modifiy the routine for oh-my-zsh so it can be installed                  (ok)
            from it's original source
         ------------------------------------------------------------
         - last -- make a routine to move all but /boot to an
                   external SSD (needs a check if an old OS exist
                   as well it keeps an existing data partiton if exists)
                   This needs a command line parameter (nand-yes/nand-no)
         ------------------------------------------------------------
         - Integrate the rest from firstlogin.sh
         - Colorize the console output                                              (ok)
         - Add option for private git repo
         - test with other platforms
         - Rename script and repo to something meaningful
         - create entries for dhcpd.conf
         - Add Install Options: Docker, OMV,
         - Run armbian-config utility and go to section system -> DTB and 
            select optimized board configuration for Odroid HC1

    Disclaimer
    ----------
    Absolutely NO support, if you use this project you're at your own!
    All and everything in this scripts is copied from the Internet with no or little modification.
    If you get stuck, do it like me and READ THE INTERNET!