#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

# Setup files and directories
# ---------------------------
SETUP_DIR=/tmp/setup                    # Temporary setup directory, needs to be removed at the end
SETUP_FLAG=/tmp/setup/setup.run         # Setup control file, needs to moved away at the end
DHCPINFO=/tmp/setup/dhcp.info           # DHCP data file
DHCLIENT_CONF=/tmp/setup/dhclient.conf  # dhclient conf file, created from this script
SETUP_LOG=/tmp/setup/setup.log          # Logfile, needs to moved away at the end

# static configuration, no need to get it from DHCP
# -------------------------------------------------
DOCKER_PKG="docker-ce docker-ce-cli"    # packages list that install Docker

# Definiton of local DHCP options
# code 224 - 250 is defined as local class range
# ----------------------------------------------
USERNAME="option local-username code 224=text;"
PASSWORD="local-passwd code 225=text;"
APT_PKG="option local-pkg code 226=text;"
NFS_SERVER_PATH="option local-server-path code 227=text;"
LOCAL_PATH="option local-nfs-path code 228=text;"

BOARD_MODEL_1="Raspberry"
BOARD_MODEL_2="Rock64"
BOARD_MODEL=
IMAGE_TYPE=""
OS_ID=
OS_VER_ID=
OS_VERSION_CODENAME=
OS_KERNEL_RELEASE=

# Some Information from DHCP
# --------------------------
HOSTNAME=
INTERFACE=
IPADDR=
SUBNET=
GATEWAY=
NAMESERVER=
DOMAIN_NAME=
DOMAIN_SEARCH=
TIMEZONE=
USERNAME=
PASSWORD=
APT_PKG=                                # package list to install
NFS_SERVER_PATH=
LOCAL_NFS_PATH=                              # where nfs should be mounted to

# command line pparameters
# ------------------------
DOCKER_INST=false                       # should docker daemon be installed? boolean
DONT_INSTALL=true

about()
{
    cat <<- EOF
    Command to quick install
    # curl -s https://gitlab.com/pascalep/home-network-setup-and-configuration/raw/master/firstlogin.sh | sudo bash


    # Next Todo's
    # -------------
    #
    # - fix dhcp requests
    # - create dhclient.conf file and look what information the
    # - DHCP server has for us
    # - output all information we gathered
    # 
    #
    # ------------------------------------------------------------
    # - last -- make a routine to move all but /boot to an
    #           external SSD (needs a check if an old OS exist
    #           as well it keeps data partiton )
    # ------------------------------------------------------------
EOF
}

#######################
# Let's the fun begin #
#######################

if_root_check() {
# Make sure only root can run our script
    if [[ $EUID -ne 0 ]]; then
        echo "#########################################"
        echo "# You must be root to run this script.  #"
        echo "# use sudo if possible                  #"
        echo "#########################################" 1>&2
        exit 1
    fi
}

hardware_check() {
    # Check for the board version
    # For Raspberry, gives the Hardware Revision Code
    # this is not used at the moment
    # cat /proc/cpuinfo | grep 'Revision' | awk '{print $3}'
    # Instead we use this dirty method that only returns Raspberry or Rock64
    if [ "$BOARD_MODEL_1" == $(cat /proc/device-tree/model | awk '{print $1}') ]; then
        BOARD_MODEL=$BOARD_MODEL_1
    elif [ "$BOARD_MODEL_2" == $(cat /proc/device-tree/model | awk '{print $2}') ]; then
        BOARD_MODEL=$BOARD_MODEL_2
    else
        echo "############################"
        echo "# Your Hardware is unknown #"
        echo "############################"
        echo ""
        read -p "Press enter to continue"
        DONT_INSTALL=true
    fi
}

check_os() {
    # Some more useful information
    # I can't find a way to determine the exact image installed
    # Hopefully we can work with minor information
    OS_ID=$(cat /etc/os-release | grep '^ID=' |  sed -e 's/ID=//')
    OS_VER_ID=$(cat /etc/os-release | grep 'VERSION_ID=' | sed 's/VERSION_ID=//;s/"//g')
    OS_VERSION_CODENAME=$(cat /etc/os-release | grep 'VERSION_CODENAME' | sed 's/VERSION_CODENAME=//')
    OS_KERNEL_RELEASE=$(uname -r)

    # What can we found about the installed OS
    IMAGE_TYPE=$(cat /etc/os-release | grep -i 'hypriot_image_ver' | sed 's/=/ /;s/"//g' | awk '{print $2}')
    if [ ! -z "$IMAGE_TYPE" ]; then
        IMAGE_TYPE="HypriotOS_$IMAGE_TYPE"
    elif IMAGE_TYPE=$(uname -r | grep -i 'ayufan' | sed 's/-/ /g' | awk '{print $4}') || [ ! -z "$IMAGE_TYPE" ]; then 
        str1=$(uname -r | grep -i 'ayufan' | sed 's/-/ /g' | awk '{print $1}')
        str2=$(uname -r | grep -i 'ayufan' | sed 's/-/ /g' | awk '{print $2}')
        if [ "$IMAGE_TYPE" = "ayufan" ]; then
            if [ -x "$(command -v docker)" ]; then
                IMAGE_TYPE="$IMAGE_TYPE-Docker-$str1-$str2"
            else
                IMAGE_TYPE="$IMAGE_TYPE-$str1-$str2"
            fi 
        fi
    fi
    if [  -z "$IMAGE_TYPE" ]; then
        IMAGE_TYPE=$(cat /etc/os-release | grep -i 'PRETTY_NAME' | sed 's/PRETTY_NAME=//;s/"//g')
        if [ -x "$(command -v docker)" ]; then
            IMAGE_TYPE="$IMAGE_TYPE-Docker"
        fi
    fi
}
# until now, all only carred about information, the system has been touched at all
# Here is what we know until now.

check_docker() {
    # Change a flag if Docker is already installed
    if [ -x "$(command -v docker)" ]; then
        DOCKER_INST="installed"
    fi
}

setup_dir() {
    # If setup dir does not exist create it
    if [ ! -d "$SETUP_DIR" ]; then
        mkdir -p $SETUP_DIR
    fi

    # if dummy file does not exist create it
    if [ -f "$SETUP_FLAG" ]; then
        echo "Another setup has not completed. Try a fresh install"
        echo "Setup will now abort!"
        exit 0
    else
        touch $SETUP_FLAG
    fi
    # Create a Logfile and begin installation
    if [ ! -f "$SETUP_LOG" ]; then
        touch $SETUP_LOG
        echo "Setup has begun" >> $SETUP_LOG
    fi
}

dhcp_info() {
    # if dhcp info file already exists delete it
    if [ -f "$DHCPINFO" ]; then
        rm $DHCPINFO
    fi

    #Create dhclient-conf file for setup
    if [ -f "$DHCLIENT_CONF" ]; then
        rm $DHCLIENT_CONF
    fi
    curl -s https://gitlab.com/pascalep/home-network-setup-and-configuration/raw/master/dhclient.conf -o $DHCLIENT_CONF
    dhclient -cf $DHCLIENT_CONF -lf $DHCPINFO -sf $SETUP_FLAG

    # This should to the job

    HOSTNAME=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'server-name' | awk '{print $2}' | sed -e 's/;//;s/"//g')
    INTERFACE=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'interface' | awk '{print $2}' | sed -e 's/;//;s/"//g')
    IPADDR=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'fixed-address' | awk '{print $2}' | sed -e 's/;//')
    SUBNET=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'subnet-mask' | awk '{print $3}' | sed -e 's/;//')
    GATEWAY=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'routers' | awk '{print $3}' | sed -e 's/;//')
    NAMESERVER=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'domain-name-servers' | awk '{print $3}' | sed -e 's/;//;s/,/ /g')
    DOMAIN_NAME=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'option domain-name ' | awk '{print $3}' | sed -e 's/;//;s/"//g')
    DOMAIN_SEARCH=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'domain-search' | awk '{print $3}' | sed -e 's/;//;s/"//g')
    TIMEZONE=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'tcode' | awk '{print $3}' | sed -e 's/;//;s/"//g')
    USERNAME=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'local-username' | awk '{print $3}' | sed -e 's/;//;s/"//g')
    PASSWORD=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'local-passwd' | awk '{print $3}' | sed -e 's/;//;s/"//g')
    APT_PKG=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'local-pkg' | sed -e 's/option local-pkg "//;s/;//;s/"//g;s/\\012//g;s/^[ \t]*//;s/  */ /g')
    NFS_SERVER_PATH=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'local-server-path' | awk '{print $3}' | sed -e 's/;//;s/"//g')
    LOCAL_NFS_PATH=$(tail -n 25 $DHCPINFO | grep '-m' '1' 'local-nfs-path' | awk '{print $3}' | sed -e 's/;//;s/"//g')

    # This is for information during scripting only!
    echo ""
    echo "Some Information from DHCP"
    echo "--------------------------"
    echo "Hostname:        $HOSTNAME"
    echo "Interface:       $INTERFACE"
    echo "IP address:      $IPADDR"
    echo "Subnet:          $SUBNET"
    echo "Gateway:         $GATEWAY"
    echo "DNS Server:      $NAMESERVER"
    echo "DNS Domain:      $DOMAIN_NAME"
    echo "DNS Search:      $DOMAIN_SEARCH"
    echo "Timezone:        $TIMEZONE"
    echo "Username:        $USERNAME"
    echo "Password:        $PASSWORD"
    echo "Packages inst:   $APT_PKG"
    echo "NFS Server path: $NFS_SERVER_PATH"
    echo "Local NFS path:  $LOCAL_NFS_PATH"
    echo ""
    echo "Setup files and directories"
    echo "---------------------------"
    echo "Temporary setup directory: $SETUP_DIR"
    echo "Setup control file:        $SETUP_FLAG"
    echo "DHCP data file:            $DHCPINFO"
    echo "dhclient conf file:        $DHCLIENT_CONF"
    echo "Logfile:                   $SETUP_LOG"
    echo "Pkg for Docker:            $DOCKER_PKG"
    echo ""
    echo "Some system information"
    echo "-----------------------"
    echo "Docker Installation:         $DOCKER_INST"
    echo "Boards known to this script: $BOARD_MODEL_1, $BOARD_MODEL_2"
    echo "Boardmodel:                  $BOARD_MODEL"
    echo "Installed Image Version:     $IMAGE_TYPE"
    echo "Operating system Type:       $OS_ID"
    echo "Operating System Version:    $OS_VER_ID"
    echo "Operating System Code Name:  $OS_VERSION_CODENAME"
    echo "Kernel Version:              $OS_KERNEL_RELEASE"
}

main() {
    # remove after testing
    if [ -f "$SETUP_FLAG" ]; then
        rm $SETUP_FLAG
    fi
    exit 0
}

usage() {
    cat <<- EOF
    usage: $PROGNAME options
    
    Program deletes files from filesystems to release space. 
    It gets config file that define fileystem paths to work on, and whitelist rules to 
    keep certain files.

    OPTIONS:
       -c --config              configuration file containing the rules. use --help-config to see the syntax.
       -n --pretend             do not really delete, just how what you are going to do.
       -t --test                run unit test to check the program
       -v --verbose             Verbose. You can specify more then one -v to have more verbose
       -x --debug               debug
       -h --help                show this help
          --help-config         configuration help

    
    Examples:
       Run all tests:
       $PROGNAME --test all

       Run specific test:
       $PROGNAME --test test_string.sh

       Run:
       $PROGNAME --config /path/to/config/$PROGNAME.conf

       Just show what you are going to do:
       $PROGNAME -vn -c /path/to/config/$PROGNAME.conf
EOF
}

cmdline() {
    # got this idea from here:
    # http://kirk.webfinish.com/2009/10/bash-shell-script-to-use-getopts-with-gnu-style-long-positional-parameters/
    local arg=
    for arg
    do
        local delim=""
        case "$arg" in
            #translate --gnu-long-options to -g (short options)
            --config)         args="${args}-c ";;
            --pretend)        args="${args}-n ";;
            --test)           args="${args}-t ";;
            --help-config)    usage_config && exit 0;;
            --help)           args="${args}-h ";;
            --verbose)        args="${args}-v ";;
            --debug)          args="${args}-x ";;
            #pass through anything else
            *) [[ "${arg:0:1}" == "-" ]] || delim="\""
                args="${args}${delim}${arg}${delim} ";;
        esac
    done

    #Reset the positional parameters to the short options
    eval set -- $args

    while getopts "nvhxt:c:" OPTION
    do
         case $OPTION in
         v)
             readonly VERBOSE=1
             ;;
         h)
             usage
             exit 0
             ;;
         x)
             readonly DEBUG='-x'
             set -x
             ;;
         t)
             RUN_TESTS=$OPTARG
             verbose VINFO "Running tests"
             ;;
         c)
             readonly CONFIG_FILE=$OPTARG
             ;;
         n)
             readonly PRETEND=1
             ;;
        esac
    done

    if [[ $recursive_testing || -z $RUN_TESTS ]]; then
        [[ ! -f $CONFIG_FILE ]] \
            && exit 0 "You must provide --config file"
    fi
    return 0
}

main1() {
    cmdline $ARGS
}
main1 "$@"