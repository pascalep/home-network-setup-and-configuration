- problem with asound.conf
- remove old unnessecary comments (partly done)
- ~~add hostname to localhost in hosts~~
- ~~add automatic yes to all in quiet mode (required apps)~~
- zshrc for root install problem
- cleanup "optimize raspbian for audiostreamer (pre ALPHA!)"
- ~~xxx cleanup "Some Information from DHCP (This is what's currently configured and can be used in the script)"~~
- Snapclient latest
- Raspotify latest
- ~~(logging not done) add message parameter to _ok, _info etc. (maybe logging directly from there)~~
- ZSH move personal configuration to custom directory

- setup_info (cleanup section with additional services)
- main (cleanup section with additional services 199-333)

- check if printf is better then echo for screen output

- update Readme with all options
- automatically ask to check for updates every xx days
- login screen with some fancy information like:
    - setup time stamp
    - last update
    - uptime
    - some sysinfo (temp, cpu usage, free disk space etc.)
    - installed and running services incl. health information
- give this scriüt a new name

create log file